<p align="center">
    <img src="https://gitlab.com/barbra-group/graphler/raw/develop/images/logo.svg" />
</p>


# graphler


> Generate & scaffold type-safe operations based on your GraphQL Schema in TypeScript

## Features

- **🚀 Schema-first:** Based on your GraphQL schema & model definitions
- **🤝 Type-safe:** Strong mapping between your GraphQL schema and queries, input arguments and models
- **🏗 Codegen & scaffolding workflows:** Minimal operations boilerplate & automatically generated type definitions

## Motivation

Programming in type-safe environments provides a lot of benefits and gives you confidence about your code. `graphler` leverages the strongly typed GraphQL schema with the goal of making your frontend type-safe while reducing the need to write boilerplate through code generation.

#### Supported languages:

- `TypeScript`
- ~~`JavaScript`~~

<p align="center">
    <img height="150px" src="https://gitlab.com/barbra-group/graphler/raw/develop/images/footer.svg" />
</p>