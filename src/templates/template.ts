import { IntrospectionInterfaceType, IntrospectionNamedTypeRef } from "graphql";

export type Template = {
  name: string;
  requires: ReadonlyArray<
    IntrospectionNamedTypeRef<IntrospectionInterfaceType>
  >;
  template: string;
};
