import {
  IntrospectionField,
  IntrospectionOutputType,
  IntrospectionEnumValue,
  IntrospectionOutputTypeRef
} from "graphql";
import { type } from "os";

type CollapsedIntrospectionType = {
  list: "none" | "nullable" | "nonNullable";
  type: { nonNullable: boolean; name: string };
};

const getCollapsedType = (
  typeIntrospection: IntrospectionOutputTypeRef,
  collapsedType: CollapsedIntrospectionType = {
    list: "none",
    type: { nonNullable: false, name: "ERROR" }
  },
  propagatedNonNullable: boolean = false
): CollapsedIntrospectionType => {
  const newCollapsedType: CollapsedIntrospectionType = {
    list:
      typeIntrospection.kind === "LIST"
        ? propagatedNonNullable
          ? "nonNullable"
          : "nullable"
        : collapsedType.list,

    type: {
      nonNullable:
        collapsedType.type.nonNullable ||
        (typeIntrospection.kind !== "LIST" && propagatedNonNullable),
      name:
        "name" in typeIntrospection && typeIntrospection.name
          ? typeIntrospection.name
          : collapsedType.type.name
    }
  };

  if (typeIntrospection.kind === "NON_NULL") {
    return getCollapsedType(typeIntrospection.ofType, newCollapsedType, true);
  } else {
    return newCollapsedType;
  }
};

export const getOutputFieldTemplate = (name: string) => (
  fieldIntrospection?: IntrospectionOutputTypeRef
) => {
  if (!fieldIntrospection) {
    // Is an enumfield
    return `${name} = "${name}",`;
  } else if ("type" in fieldIntrospection) {
    const { name, type: introspectionType } = fieldIntrospection;

    const { list, type } = getCollapsedType(introspectionType);
    console.log(getCollapsedType(introspectionType));

    // Is a queryfield
    return `${name}${list === "nonNullable" ? "" : "?"}: ${
      type.nonNullable ? type.name : `(${type.name} | undefined)`
    }${list !== "none" ? "[]" : ""};`;
  }

  throw new Error("Implement");
};
