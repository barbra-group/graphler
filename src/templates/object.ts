import { IntrospectionObjectType } from "graphql";

import { Template } from "./template";
import { getOutputFieldTemplate } from "./field";

export const getObjectTemplate = ({
  name,
  fields,
  interfaces
}: IntrospectionObjectType): Template => {
  const template = `type ${name} = {
    ${fields
      .map(field => {
        return getOutputFieldTemplate(field.name)(field.type);
      })
      .join("\n")}
  }`;

  return {
    name: name,
    requires: interfaces,
    template: ""
  };
};
