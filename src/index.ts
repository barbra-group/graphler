import path from "path";
import fs from "fs";
import minimist from "minimist";
import fetch from "node-fetch";

import { getGraphlerConfig } from "./config";
import { getObjectTemplate } from "./templates/object";
import { getSchemaIntrospection } from "./introspection";

const parameters = minimist(process.argv.slice(2));

const graphlerConfig: { schemaPath: string } = require(path.resolve(
  process.cwd(),
  "./graphler.json"
));

(async () => {
  const schemaIntrospection = await getSchemaIntrospection(
    graphlerConfig.schemaPath
  );

  const templates = schemaIntrospection.__schema.types
    .filter(introspectionType => {
      return introspectionType.name.substr(0, 2) !== "__";
    })
    .map(typeIntrospection => {
      switch (typeIntrospection.kind) {
        case "OBJECT": {
          return getObjectTemplate(typeIntrospection);
        }
      }
      return {};
    });

  console.log(templates);
})();
