import glob from "glob";
import fs from "fs";
import {
  introspectionFromSchema,
  parse,
  buildClientSchema,
  buildASTSchema
} from "graphql";

export const getSchemaIntrospection = async (schemaGlob: string) => {
  const schemaString = glob
    .sync(schemaGlob)
    .map(filePath => {
      if (fs.statSync(filePath).isDirectory()) {
        return "";
      }

      return fs.readFileSync(filePath);
    })
    .join("\n");

  const schemaNode = parse(schemaString);
  const astSchema = buildASTSchema(schemaNode);
  const introspection = introspectionFromSchema(astSchema);

  return introspection;
};
